#include "initializeData.h"
#include "requetes.h"

int main ()
{
    Table_hash_airlines thal;
    initialize_hashTable_airlines (&thal);

    Table_hash_airports thap;
    initialize_hashTable_airports (&thap);

    printf ("Chargement du programme ...\n");

    FILE *airline_file, *airport_file, *flights_file;
    airline_file = fopen ("data/airlines.csv", "r");
    airport_file = fopen ("data/airports.csv", "r");
    flights_file = fopen ("data/flights.csv", "r");

    if (airline_file != NULL)
    {
        load_airlines (&thal, airline_file);
    }

    if (airport_file != NULL)
    {
        load_airports (&thap, airport_file);
    }

    if (flights_file != NULL && (is_empty_airlines (thal) == 1 && is_empty_airports (thap) == 1))
    {
        load_flights (&thal, &thap, flights_file);
    }
    if (is_empty_airlines (thal) == 0 || is_empty_airports (thap) == 0)
    {
        printf ("Chargement des fichiers impossible, veuillez vérifier les fichiers !\n");
    }
    else
    {
        printf ("Chargement terminé !\n");

        char * requetes    = NULL;
        char * commande    = NULL;
        char * airline_id  = NULL;
        char * airport_id1 = NULL;
        char * airport_id2 = NULL;
        char * date        = NULL;
        char * time        = "";
        char * limit       = "";
        size_t len         = 0;

        while (getline (&requetes, &len, stdin) != -1)
        {
            char *requetes2 = requetes;
            commande        = strsep (&requetes2, " ");
            commande        = strtok (commande, "\n");
            if (strcmp (commande, "quit") == 0)
            {
                break;
            }
            else if (strcmp (commande, "show-airports") == 0)
            {
                airline_id = strsep (&requetes2, " \n");
                printf ("> Resultat de la requete %s %s :\n", commande, airline_id);
                printf ("\n");
                show_airports (airline_id, thal, thap);
            }
            else if (strcmp (commande, "show-airlines") == 0)
            {
                airport_id1 = strsep (&requetes2, " \n");
                printf ("> Resultat de la requete %s %s :\n", commande, airport_id1);
                printf ("\n");
                show_airlines (airport_id1, thal, thap);
            }
            else if (strcmp (commande, "show-flights") == 0)
            {
                airport_id1 = strsep (&requetes2, " \n");
                date        = strsep (&requetes2, " \n");
                time        = strsep (&requetes2, " \n");
                limit       = strsep (&requetes2, " \n");

                if (limit == NULL)
                {
                    printf ("> Resultat de la requete %s %s :\n", commande, airport_id1);
                    printf ("\n");
                    show_flights (airport_id1, thap, date, 0);
                }
                else
                {
                    if (strcmp (time, "") != 0 && strcmp (limit, "") != 0)
                    {
                        printf ("> Resultat de la requete %s %s %s %s :\n", commande, airport_id1, time, limit);
                        printf ("\n");
                        show_flights (airport_id1, thap, date, 2, time, limit);
                    }
                    if (strcmp (time, "") != 0 && strcmp (limit, "") == 0)
                    {
                        printf ("> Resultat de la requete %s %s %s :\n", commande, airport_id1, time);
                        printf ("\n");
                        show_flights (airport_id1, thap, date, 1, time);
                    }
                    if (strcmp (time, "") == 0 && strcmp (limit, "") != 0)
                    {
                        printf ("> Resultat de la requete %s %s %s :\n", commande, airport_id1, limit);
                        printf ("\n");
                        show_flights (airport_id1, thap, date, 1, limit);
                    }
                }
            }
            else if (strcmp (commande, "most-delayed-flights") == 0)
            {
                printf ("> Resultat de la requete %s :\n", commande);
                printf ("\n");
                most_delayed_flights (thal);
            }
            else if (strcmp (commande, "most-delayed-airlines") == 0)
            {
                printf ("> Resultat de la requete %s :\n", commande);
                printf ("\n");
                most_delayed_airlines (thal);
            }
            else if (strcmp (commande, "delayed-airline") == 0)
            {
                airline_id = strsep (&requetes2, " \n");

                printf ("> Resultat de la requete %s %s :\n", commande, airline_id);
                printf ("\n");
                delayed_airline (airline_id, thal);
            }
            else if (strcmp (commande, "most-delayed-airlines-at-airport") == 0)
            {
                airport_id1 = strsep (&requetes2, " \n");

                printf ("> Resultat de la requete %s %s :\n", commande, airport_id1);
                printf ("\n");
                most_delayed_airlines_at_airport (airport_id1, thal, thap);
            }
            else if (strcmp (commande, "changed-flights") == 0)
            {
                date = strsep (&requetes2, " \n");

                printf ("> Resultat de la requete %s %s :\n", commande, date);
                printf ("\n");
                changed_flights (date, thal);
            }
            else if (strcmp (commande, "avg-flight-duration") == 0)
            {
                airport_id1 = strsep (&requetes2, " \n");
                airport_id2 = strsep (&requetes2, " \n");

                printf ("> Resultat de la requete %s %s %s :\n", commande, airport_id1, airport_id2);
                printf ("\n");
                avg_flight_duration (airport_id1, airport_id2, thap);
            }
            else
            {
                printf ("Vous avez entré une mauvaise requête ! Veuillez réessayer !\n");
            }
            printf ("\n");
        }

        printf ("Le programme va prendre fin ...\n");

        free_all_airlines (&thal);
        free_all_airports (&thap);
        free (requetes);
    }

    if (is_empty_airlines (thal) == 1)
    {
        free_all_airlines (&thal);
    }
    if (is_empty_airports (thap) == 1)
    {
        free_all_airports (&thap);
    }

    fclose (airline_file);
    fclose (airport_file);
    fclose (flights_file);

    printf ("Programme terminé !\n");
    return 0;
}
