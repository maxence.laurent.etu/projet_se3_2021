void search_airline (Table_hash_airlines, char *, Liste_Airlines **);

void search_airport (Table_hash_airports, char *, Liste_Airports **);

void affiche_flight (Flight *);

void affiche_airport (Airport *);

void affiche_airline (Airline *);

void show_airports (char *, Table_hash_airlines, Table_hash_airports);

void show_airlines (char *, Table_hash_airlines, Table_hash_airports);

void show_flights (char *, Table_hash_airports, char *, int, ...);

void most_delayed_flights (Table_hash_airlines);

void tri_tab_flight (Flight *, int);

void most_delayed_airlines (Table_hash_airlines);

void tri_tab_airline (Liste_Airlines *, int);

void delayed_airline (char *, Table_hash_airlines);

int delayed_airline_arr_at_airport (Flight, char *);

void tri_tab_airline_arr (Liste_Airlines *, int, char *);

void most_delayed_airlines_at_airport (char *, Table_hash_airlines, Table_hash_airports);

void changed_flights (char *, Table_hash_airlines);

void avg_flight_duration (char *, char *, Table_hash_airports);
