#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "initializeData.h"

void search_airline (Table_hash_airlines thal, char *airline_id, Liste_Airlines **la)
{

    Liste_Airlines *l = thal.tab_airlines[hash_function_airline (airline_id)];

    while (l != NULL && strcmp (l->airline.IATA_CODE, airline_id) != 0)
    {
        l = l->suiv;
    }

    (*la) = l;
}

void search_airport (Table_hash_airports thap, char *airport_id, Liste_Airports **la)
{
    Liste_Airports *l = thap.tab_airports[hash_function_airport (airport_id)];

    while (l != NULL && strcmp (l->airport.IATA_CODE, airport_id) != 0)
    {
        l = l->suiv;
    }

    (*la) = l;
}

void affiche_flight (Flight *fl)
{
    fl->mask &(1 << 0) ? printf ("%d,", fl->month) : printf (",");
    fl->mask &(1 << 1) ? printf ("%d,", fl->day) : printf (",");
    fl->mask &(1 << 2) ? printf ("%d,", fl->weekday) : printf (",");
    fl->airline == NULL ? printf (",") : printf ("%s,", fl->airline);
    fl->org_air == NULL ? printf (",") : printf ("%s,", fl->org_air);
    fl->dest_air == NULL ? printf (",") : printf ("%s,", fl->dest_air);
    fl->mask &(1 << 3) ? printf ("%d,", fl->sched_dep) : printf (",");
    fl->mask &(1 << 4) ? printf ("%0.2f,", fl->dep_delay) : printf (",");
    fl->mask &(1 << 5) ? printf ("%0.2f,", fl->air_time) : printf (",");
    fl->mask &(1 << 6) ? printf ("%d,", fl->dist) : printf (",");
    fl->mask &(1 << 7) ? printf ("%d,", fl->sched_arr) : printf (",");
    fl->mask &(1 << 8) ? printf ("%0.2f,", fl->arr_delay) : printf (",");
    fl->mask &(1 << 9) ? printf ("%d,", fl->diverted) : printf (",");
    fl->mask &(1 << 10) ? printf ("%d\n", fl->cancelled) : printf ("\n");
}

void affiche_airport (Airport *ap)
{
    ap->IATA_CODE == NULL ? printf ("NULL,") : printf ("%s,", ap->IATA_CODE);
    ap->airport == NULL ? printf ("NULL\n") : printf ("%s\n", ap->airport);
}

void affiche_airline (Airline *al)
{
    al->IATA_CODE == NULL ? printf ("NULL,") : printf ("%s,", al->IATA_CODE);
    al->airline == NULL ? printf ("NULL") : printf ("%s", al->airline);
}

void show_airports (char *airline_id, Table_hash_airlines thal, Table_hash_airports thap)
{
    Liste_Airlines *lal = NULL;
    search_airline (thal, airline_id, &lal);

    Airport tab[AIRPORT_SIZE_TABLE];
    int     nb_elt_airport = 0;
    int     i              = 0;
    int     res            = 1;

    Flight *f = lal->liste_flights;
    while (f != NULL)
    {
        Liste_Airports *lap = NULL;
        while (i < nb_elt_airport && res == 1)
        {
            if (strcmp (f->org_air, tab[i].IATA_CODE) == 0)
            {
                res = 0;
            }
            i++;
        }
        if (res == 1)
        {
            search_airport (thap, f->org_air, &lap);
            affiche_airport (&lap->airport);
            tab[nb_elt_airport] = lap->airport;
            nb_elt_airport++;
        }
        i   = 0;
        res = 1;
        f   = f->suiv_airline;
    }
}

void show_airlines (char *airport_id, Table_hash_airlines thal, Table_hash_airports thap)
{
    Liste_Airports *lap = NULL;
    search_airport (thap, airport_id, &lap);

    Airline tab[AIRLINE_SIZE_TABLE];
    int     nb_elt_airline = 0;
    int     i              = 0;
    int     res            = 1;

    Flight *f = lap->liste_flights;
    while (f != NULL)
    {
        Liste_Airlines *lal = NULL;
        while (i < nb_elt_airline && res == 1)
        {
            if (strcmp (f->airline, tab[i].IATA_CODE) == 0)
            {
                res = 0;
            }
            i++;
        }
        if (res == 1)
        {
            search_airline (thal, f->airline, &lal);
            affiche_airline (&lal->airline);
            tab[nb_elt_airline] = lal->airline;
            nb_elt_airline++;
        }
        i   = 0;
        res = 1;
        f   = f->suiv_airport;
    }
}

void show_flights (char *airport_id, Table_hash_airports thap, char *date, int nb, ...)
{
    Liste_Airports *lap = NULL;
    search_airport (thap, airport_id, &lap);

    va_list ap;
    va_start (ap, nb);
    char *strToken = strtok (date, "-");
    int   mois     = atoi (strToken);
    strToken       = strtok (NULL, "-");
    int jour       = atoi (strToken);

    Flight *pf = lap->liste_flights;
    while (pf != NULL && (pf->month != mois || pf->day != jour))
    {
        pf = pf->suiv_airport;
    }

    if (nb == 0)
    {
        while (pf != NULL && pf->month == mois && pf->day == jour)
        {
            affiche_flight (pf);
            pf = pf->suiv_airport;
        }
    }
    else if (nb == 1)
    {

        char *arg = va_arg (ap, char *);

        if (strlen (arg) > 4)
        {
            int limit = atoi (strtok (arg, "limit="));

            while (pf != NULL && pf->month == mois && pf->day == jour && limit > 0)
            {
                affiche_flight (pf);
                pf = pf->suiv_airport;
                limit--;
            }
        }
        else
        {
            int time = atoi (arg);

            while (pf != NULL && pf->month == mois && pf->day == jour)
            {
                if (pf->sched_dep >= time)
                {
                    affiche_flight (pf);
                }

                pf = pf->suiv_airport;
            }
        }
    }
    else if (nb == 2)
    {
        char *arg1  = va_arg (ap, char *);
        int   time  = atoi (arg1);
        char *arg2  = va_arg (ap, char *);
        int   limit = atoi (strtok (arg2, "limit="));

        while (pf != NULL && pf->month == mois && pf->day == jour && limit > 0)
        {
            if (pf->sched_dep >= time)
            {
                affiche_flight (pf);
                limit--;
            }
            pf = pf->suiv_airport;
        }
    }
    va_end (ap);
}

void tri_tab_flight (Flight *tab, int nb_elt_flight)
{
    for (int i = 0; i < nb_elt_flight - 1; i++)
    {
        for (int j = 0; j < nb_elt_flight - i - 1; j++)
        {
            if (tab[j].arr_delay < tab[j + 1].arr_delay)
            {
                Flight tmp = tab[j];
                tab[j]     = tab[j + 1];
                tab[j + 1] = tmp;
            }
        }
    }
}

void most_delayed_flights (Table_hash_airlines thal)
{
    Flight tab[5];
    int    nb_elt_flight = 0;

    for (int i = 0; i < AIRLINE_SIZE_TABLE; i++)
    {
        if (thal.tab_airlines[i] != NULL)
        {
            Liste_Airlines *la = thal.tab_airlines[i];
            while (la != NULL)
            {
                Flight *p = la->liste_flights;
                while (p != NULL)
                {
                    if (nb_elt_flight < 5)
                    {
                        tab[nb_elt_flight] = *p;
                        nb_elt_flight++;
                        tri_tab_flight (tab, nb_elt_flight);
                    }
                    else
                    {
                        int j = 5;
                        while (j > 0 && p->arr_delay > tab[j - 1].arr_delay)
                        {
                            j--;
                        }

                        if (j < 5)
                        {
                            tab[j] = *p;
                        }
                    }
                    p = p->suiv_airline;
                }
                la = la->suiv;
            }
        }
    }

    for (int i = 0; i < nb_elt_flight; i++)
    {
        affiche_flight (&tab[i]);
    }
}

int delayed_airline_dep_arr (Flight *fl)
{
    int sum        = 0;
    int nb_flights = 0;
    while (fl != NULL)
    {
        sum += fl->dep_delay + fl->arr_delay;
        nb_flights++;
        fl = fl->suiv_airline;
    }

    return sum / nb_flights;
}

void tri_tab_airline (Liste_Airlines *tab, int nb_elt_airline)
{
    for (int i = 0; i < nb_elt_airline - 1; i++)
    {
        for (int j = 0; j < nb_elt_airline - i - 1; j++)
        {
            if (delayed_airline_dep_arr (tab[j].liste_flights) < delayed_airline_dep_arr (tab[j + 1].liste_flights))
            {
                Liste_Airlines tmp = tab[j];
                tab[j]             = tab[j + 1];
                tab[j + 1]         = tmp;
            }
        }
    }
}

void most_delayed_airlines (Table_hash_airlines thal)
{
    Liste_Airlines tab[5];
    int            nb_elt_airline = 0;

    for (int i = 0; i < AIRLINE_SIZE_TABLE; i++)
    {
        if (thal.tab_airlines[i] != NULL)
        {
            Liste_Airlines *la = thal.tab_airlines[i];
            while (la != NULL)
            {
                if (nb_elt_airline < 5)
                {
                    tab[nb_elt_airline] = *la;
                    nb_elt_airline++;
                    tri_tab_airline (tab, nb_elt_airline);
                }
                else
                {
                    int j = 5;
                    while (j > 0 && delayed_airline_dep_arr (la->liste_flights) >
                                    delayed_airline_dep_arr (tab[j - 1].liste_flights))
                    {
                        j--;
                    }

                    if (j < 5)
                    {
                        tab[j] = *la;
                    }
                }
                la = la->suiv;
            }
        }
    }

    for (int i = 0; i < nb_elt_airline; i++)
    {
        affiche_airline (&tab[i].airline);
    }
}

void delayed_airline (char *airline_id, Table_hash_airlines thal)
{
    Liste_Airlines *lal = NULL;
    search_airline (thal, airline_id, &lal);

    int res = delayed_airline_dep_arr (lal->liste_flights);

    printf ("%s,%s,%d\n", lal->airline.IATA_CODE, lal->airline.airline, res);
}

int delayed_airline_arr_at_airport (Flight *fl, char *airport_id)
{
    int sum        = 0;
    int nb_flights = 0;
    while (fl != NULL)
    {
        if (strcmp (fl->dest_air, airport_id) == 0)
        {
            sum += fl->dep_delay + fl->arr_delay;
            nb_flights++;
        }
        fl = fl->suiv_airline;
    }

    return sum / nb_flights;
}

void tri_tab_airline_arr (Liste_Airlines *tab, int nb_elt_airline, char *airport_id)
{
    for (int i = 0; i < nb_elt_airline - 1; i++)
    {
        for (int j = 0; j < nb_elt_airline - i - 1; j++)
        {
            if (delayed_airline_arr_at_airport (tab[j].liste_flights, airport_id) <
                delayed_airline_arr_at_airport (tab[j + 1].liste_flights, airport_id))
            {
                Liste_Airlines tmp = tab[j];
                tab[j]             = tab[j + 1];
                tab[j + 1]         = tmp;
            }
        }
    }
}

void most_delayed_airlines_at_airport (char *airport_id, Table_hash_airlines thal, Table_hash_airports thap)
{
    Liste_Airports *lap = NULL;
    search_airport (thap, airport_id, &lap);

    Liste_Airlines tab[3];
    int            nb_elt_airline = 0;

    Flight *fl = lap->liste_flights;
    while (fl != NULL)
    {
        Liste_Airlines *lal = NULL;
        search_airline (thal, fl->airline, &lal);

        if (nb_elt_airline < 3)
        {
            tab[nb_elt_airline] = *lal;
            nb_elt_airline++;
            tri_tab_airline_arr (tab, nb_elt_airline, airport_id);
        }
        else
        {
            bool trouver = false;

            for (int l = 0; l < nb_elt_airline; l++)
            {
                if (strcmp (fl->airline, tab[l].airline.IATA_CODE) == 0)
                {
                    trouver = true;
                }
            }

            if (trouver == false)
            {
                int j = 3;
                while (j > 0 && delayed_airline_dep_arr (fl) > delayed_airline_dep_arr (tab[j - 1].liste_flights))
                {
                    j--;
                }

                if (j < 3)
                {
                    tab[j] = *lal;
                }
            }
        }
        fl = fl->suiv_airport;
    }

    for (int i = 0; i < nb_elt_airline; i++)
    {
        affiche_airline (&tab[i].airline);
    }
}


void changed_flights (char *date, Table_hash_airlines thal)
{

    char *strToken = strtok (date, "-");
    int   mois     = atoi (strToken);
    strToken       = strtok (NULL, "-");
    int jour       = atoi (strToken);

    for (int i = 0; i < AIRLINE_SIZE_TABLE; i++)
    {
        if (thal.tab_airlines[i] != NULL)
        {
            Liste_Airlines *la = thal.tab_airlines[i];
            while (la != NULL)
            {
                Flight *p = la->liste_flights;
                while (p != NULL)
                {
                    if (p->month == mois && p->day == jour && (p->diverted == 1 || p->cancelled == 1))
                    {
                        affiche_flight (p);
                    }
                    p = p->suiv_airline;
                }
                la = la->suiv;
            }
        }
    }
}

void avg_flight_duration (char *airport_id_1, char *airport_id_2, Table_hash_airports thap)
{
    Liste_Airports *lap = NULL;
    search_airport (thap, airport_id_1, &lap);

    double sum        = 0;
    int    nb_flights = 0;

    Flight *fl = lap->liste_flights;
    while (fl != NULL)
    {
        if (strcmp (fl->dest_air, airport_id_2) == 0)
        {
            sum += fl->air_time;
            nb_flights++;
        }
        fl = fl->suiv_airport;
    }

    printf ("average: %.2f minutes (%d flights)\n", sum / nb_flights, nb_flights);
}
