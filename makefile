CC=clang # compilateur
CFLAGS=-Wall -Wextra -g # warnings

EXEC=main

all: $(EXEC)

main: main.o initializeData.o requetes.o
	$(CC) -o $@ $^

main.o: main.c initializeData.h requetes.h
	$(CC) $(CFLAGS) -c $< -o $@

initializeData.o: initializeData.c
	$(CC) $(CFLAGS) -c $< -o $@

requetes.o: requetes.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm -f *.o core

mrproper: clean
	rm -f main
