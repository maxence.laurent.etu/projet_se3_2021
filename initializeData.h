#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define AIRLINE_SIZE_TABLE 100
#define AIRPORT_SIZE_TABLE 500

// Déclaration des typedef :
typedef struct airline Airline;
typedef struct airport Airport;
typedef struct flight  Flight;

typedef struct liste_airlines Liste_Airlines;
typedef struct liste_airports Liste_Airports;

typedef struct table_hash_airlines Table_hash_airlines;
typedef struct table_hash_airports Table_hash_airports;

// Déclaration des structures :
struct airline
{
    char *IATA_CODE;
    char *airline;
};

struct airport
{
    char * IATA_CODE;
    char * airport;
    char * city;
    char * state;
    char * country;
    double latitude;
    double longitude;

    int mask;
};

struct flight
{
    int    month;
    int    day;
    int    weekday;
    char * airline;
    char * org_air;
    char * dest_air;
    int    sched_dep;
    double dep_delay;
    double air_time;
    int    dist;
    int    sched_arr;
    double arr_delay;
    int    diverted;
    int    cancelled;

    long mask;

    Flight *suiv_airline;
    Flight *suiv_airport;
};

struct liste_airlines
{
    Airline         airline;
    Liste_Airlines *suiv;
    Flight *        liste_flights;
};

struct liste_airports
{
    Airport         airport;
    Liste_Airports *suiv;
    Flight *        liste_flights;
};

struct table_hash_airlines
{
    Liste_Airlines *tab_airlines[AIRLINE_SIZE_TABLE];
};

struct table_hash_airports
{
    Liste_Airports *tab_airports[AIRPORT_SIZE_TABLE];
};

// Création d'une cellule airline
void create_airline (Airline *, char **, const char *);

// Création d'une cellule airport
void create_airport (Airport *, char **, const char *);

// Création d'une cellule flight
void create_flight (Flight **, char **, const char *);

// récupère les données du fichier airlines
void load_airlines (Table_hash_airlines *, FILE *);

// récupère les données du fichier airports
void load_airports (Table_hash_airports *, FILE *);

// récupère les données du fichier flights
void load_flights (Table_hash_airlines *, Table_hash_airports *, FILE *);

// initialise la liste des flights
void initialize_hashTable_airlines (Table_hash_airlines *);

void initialize_hashTable_airports (Table_hash_airports *);

int hash_function_airline (char *);

int hash_function_airport (char *);

void free_all_airlines (Table_hash_airlines *);

void free_all_airports (Table_hash_airports *);

int is_empty_airlines (Table_hash_airlines);

int is_empty_airports (Table_hash_airports);
