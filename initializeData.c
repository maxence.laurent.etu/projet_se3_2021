#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "initializeData.h"

void create_airline (Airline *al, char **line, const char separators[])
{
    char *word = strsep (line, separators);
    strcmp (word, "") == 0 ? (al->IATA_CODE = NULL) : (al->IATA_CODE = word);
    word = strsep (line, separators);
    strcmp (word, "") == 0 ? (al->airline = NULL) : (al->airline = word);
}

void create_airport (Airport *ap, char **line, const char separators[])
{
    ap->mask = 0b00000000;

    char *word = strsep (line, separators);
    strcmp (word, "") == 0 ? (ap->IATA_CODE = NULL) : (ap->IATA_CODE = word);

    word = strsep (line, separators);
    strcmp (word, "") == 0 ? (ap->airport = NULL) : (ap->airport = word);

    word = strsep (line, separators);
    strcmp (word, "") == 0 ? (ap->city = NULL) : (ap->city = word);

    word = strsep (line, separators);
    strcmp (word, "") == 0 ? (ap->state = NULL) : (ap->state = word);

    word = strsep (line, separators);
    strcmp (word, "") == 0 ? (ap->country = NULL) : (ap->country = word);

    word = strsep (line, separators);
    if (strcmp (word, "") == 0)
    {
        (ap->latitude = -1.0);
    }
    else
    {
        ap->latitude = atof (word);
        ap->mask     = ap->mask | (1 << 0);
    }
    word = strsep (line, separators);
    if (strcmp (word, "") == 0)
    {
        (ap->longitude = -1.0);
    }
    else
    {
        ap->longitude = atof (word);
        ap->mask      = ap->mask | (1 << 1);
    }
}

void create_flight (Flight **fl, char **line, const char separators[])
{
    (*fl)->mask = 0b0000000000000000;

    char *word = strsep (line, separators);
    if (strcmp (word, "") == 0)
    {
        (*fl)->month = -1.0;
    }
    else
    {
        (*fl)->month = atoi (word);
        (*fl)->mask  = (*fl)->mask | (1 << 0);
    }
    word = strsep (line, separators);
    if (strcmp (word, "") == 0)
    {
        (*fl)->day = -1.0;
    }
    else
    {
        (*fl)->day  = atoi (word);
        (*fl)->mask = (*fl)->mask | (1 << 1);
    }
    word = strsep (line, separators);
    if (strcmp (word, "") == 0)
    {
        (*fl)->weekday = -1.0;
    }
    else
    {
        (*fl)->weekday = atoi (word);
        (*fl)->mask    = (*fl)->mask | (1 << 2);
    }
    word = strsep (line, separators);
    if (strcmp (word, "") == 0)
    {
        (*fl)->airline = NULL;
    }
    else
    {
        (*fl)->airline = strdup (word);
    }
    word = strsep (line, separators);
    if (strcmp (word, "") == 0)
    {
        (*fl)->org_air = NULL;
    }
    else
    {
        (*fl)->org_air = strdup (word);
    }
    word = strsep (line, separators);
    if (strcmp (word, "") == 0)
    {
        (*fl)->dest_air = NULL;
    }
    else
    {
        (*fl)->dest_air = strdup (word);
    }
    word = strsep (line, separators);
    if (strcmp (word, "") == 0)
    {
        (*fl)->sched_dep = -1.0;
    }
    else
    {
        (*fl)->sched_dep = atoi (word);
        (*fl)->mask      = (*fl)->mask | (1 << 3);
    }
    word = strsep (line, separators);
    if (strcmp (word, "") == 0)
    {
        (*fl)->dep_delay = -1.0;
    }
    else
    {
        (*fl)->dep_delay = atof (word);
        (*fl)->mask      = (*fl)->mask | (1 << 4);
    }
    word = strsep (line, separators);
    if (strcmp (word, "") == 0)
    {
        (*fl)->air_time = -1.0;
    }
    else
    {
        (*fl)->air_time = atof (word);
        (*fl)->mask     = (*fl)->mask | (1 << 5);
    }
    word = strsep (line, separators);
    if (strcmp (word, "") == 0)
    {
        (*fl)->dist = -1.0;
    }
    else
    {
        (*fl)->dist = atoi (word);
        (*fl)->mask = (*fl)->mask | (1 << 6);
    }
    word = strsep (line, separators);
    if (strcmp (word, "") == 0)
    {
        (*fl)->sched_arr = -1.0;
    }
    else
    {
        (*fl)->sched_arr = atoi (word);
        (*fl)->mask      = (*fl)->mask | (1 << 7);
    }
    word = strsep (line, separators);
    if (strcmp (word, "") == 0)
    {
        (*fl)->arr_delay = -1.0;
    }
    else
    {
        (*fl)->arr_delay = atof (word);
        (*fl)->mask      = (*fl)->mask | (1 << 8);
    }
    word = strsep (line, separators);
    if (strcmp (word, "") == 0)
    {
        (*fl)->diverted = -1.0;
    }
    else
    {
        (*fl)->diverted = atoi (word);
        (*fl)->mask     = (*fl)->mask | (1 << 9);
    }
    word = strsep (line, separators);
    if (strcmp (word, "") == 0)
    {
        (*fl)->cancelled = -1.0;
    }
    else
    {
        (*fl)->cancelled = atoi (word);
        (*fl)->mask      = (*fl)->mask | (1 << 10);
    }

    (*fl)->suiv_airline = NULL;
    (*fl)->suiv_airport = NULL;
}

void load_airlines (Table_hash_airlines *thal, FILE *fd)
{
    char *      line       = NULL;
    size_t      len        = 0;
    const char *separators = ",;";

    getline (&line, &len, fd); // on élimine l'entête du fichier

    while (getline (&line, &len, fd) != -1) // on récupère chaque ligne.
    {
        Airline new_airline;
        create_airline (&new_airline, &line, separators);

        Liste_Airlines *new_list_airlines = malloc (sizeof (Liste_Airlines));

        new_list_airlines->airline       = new_airline;
        new_list_airlines->suiv          = NULL;
        new_list_airlines->liste_flights = NULL;

        if (thal->tab_airlines[hash_function_airline (new_airline.IATA_CODE)] == NULL)
        {
            thal->tab_airlines[hash_function_airline (new_airline.IATA_CODE)] = new_list_airlines;
        }
        else
        {
            new_list_airlines->suiv = thal->tab_airlines[hash_function_airline (new_airline.IATA_CODE)];
            thal->tab_airlines[hash_function_airline (new_airline.IATA_CODE)] = new_list_airlines;
        }
    }
    free (line);
}

void load_airports (Table_hash_airports *thap, FILE *fd)
{
    char *      line       = NULL;
    size_t      len        = 0;
    const char *separators = ",;";

    getline (&line, &len, fd); // on élimine l'entête du fichier

    while (getline (&line, &len, fd) != -1) // on récupère chaque ligne.
    {
        Airport new_airport;
        create_airport (&new_airport, &line, separators);

        Liste_Airports *new_list_airports = malloc (sizeof (Liste_Airports));

        new_list_airports->airport       = new_airport;
        new_list_airports->suiv          = NULL;
        new_list_airports->liste_flights = NULL;

        if (thap->tab_airports[hash_function_airport (new_airport.IATA_CODE)] == NULL)
        {
            thap->tab_airports[hash_function_airport (new_airport.IATA_CODE)] = new_list_airports;
        }
        else
        {
            new_list_airports->suiv = thap->tab_airports[hash_function_airport (new_airport.IATA_CODE)];
            thap->tab_airports[hash_function_airport (new_airport.IATA_CODE)] = new_list_airports;
        }
    }
    free (line);
}

void load_flights (Table_hash_airlines *thal, Table_hash_airports *thap, FILE *fd)
{
    char *      line       = NULL;
    size_t      len        = 0;
    const char *separators = ",;";

    getline (&line, &len, fd); // on élimine l'entête du fichier

    while (getline (&line, &len, fd) != -1) // on récupère chaque ligne.
    {
        char *  line2      = line;
        Flight *new_flight = malloc (sizeof (Flight));
        create_flight (&new_flight, &line2, separators);

        Liste_Airlines *l = thal->tab_airlines[hash_function_airline (new_flight->airline)];

        while (l != NULL && strcmp (l->airline.IATA_CODE, new_flight->airline) != 0)
        {
            l = l->suiv;
        }

        Liste_Airports *lp = thap->tab_airports[hash_function_airport (new_flight->org_air)];

        while (lp != NULL && strcmp (lp->airport.IATA_CODE, new_flight->org_air) != 0)
        {
            lp = lp->suiv;
        }

        if (l->liste_flights == NULL)
        {
            l->liste_flights = new_flight;
        }
        else
        {
            new_flight->suiv_airline = l->liste_flights;
            l->liste_flights         = new_flight;
        }

        if (lp->liste_flights == NULL)
        {
            lp->liste_flights = new_flight;
        }
        else
        {
            new_flight->suiv_airport = lp->liste_flights;
            lp->liste_flights        = new_flight;
        }
    }
    free (line);
}

int hash_function_airline (char *iata_code)
{
    int result = 0;
    int i      = 0;

    while (iata_code[i] != '\0')
    {
        result += iata_code[i];
        i++;
    }

    return result % AIRLINE_SIZE_TABLE;
}

int hash_function_airport (char *iata_code)
{
    int result = 0;
    int i      = 0;

    while (iata_code[i] != '\0')
    {
        result += iata_code[i];
        i++;
    }

    return result % AIRPORT_SIZE_TABLE;
}

void initialize_hashTable_airlines (Table_hash_airlines *thal)
{
    for (int i = 0; i < AIRLINE_SIZE_TABLE; i++)
    {
        thal->tab_airlines[i] = NULL;
    }
}

void initialize_hashTable_airports (Table_hash_airports *thap)
{
    for (int i = 0; i < AIRPORT_SIZE_TABLE; i++)
    {
        thap->tab_airports[i] = NULL;
    }
}

void free_all_airlines (Table_hash_airlines *thal)
{
    for (int i = 0; i < AIRLINE_SIZE_TABLE; i++)
    {
        if (thal->tab_airlines[i] != NULL)
        {
            while (thal->tab_airlines[i] != NULL)
            {
                while (thal->tab_airlines[i]->liste_flights != NULL)
                {
                    if (thal->tab_airlines[i]->liste_flights->suiv_airline == NULL)
                    {
                        Flight *tmp                          = thal->tab_airlines[i]->liste_flights;
                        thal->tab_airlines[i]->liste_flights = NULL;
                        free (tmp->airline);
                        free (tmp->org_air);
                        free (tmp->dest_air);
                        free (tmp);
                    }
                    else
                    {
                        Flight *tmp                          = thal->tab_airlines[i]->liste_flights;
                        thal->tab_airlines[i]->liste_flights = thal->tab_airlines[i]->liste_flights->suiv_airline;
                        tmp->suiv_airline                    = NULL;
                        free (tmp->airline);
                        free (tmp->org_air);
                        free (tmp->dest_air);
                        free (tmp);
                    }
                }
                Liste_Airlines *tmp   = thal->tab_airlines[i];
                thal->tab_airlines[i] = thal->tab_airlines[i]->suiv;
                tmp->suiv             = NULL;
                free (tmp->airline.IATA_CODE);
                free (tmp);
            }
        }
    }
}

void free_all_airports (Table_hash_airports *thap)
{
    for (int i = 0; i < AIRPORT_SIZE_TABLE; i++)
    {
        if (thap->tab_airports[i] != NULL)
        {
            while (thap->tab_airports[i] != NULL)
            {
                Liste_Airports *tmp   = thap->tab_airports[i];
                thap->tab_airports[i] = thap->tab_airports[i]->suiv;
                tmp->suiv             = NULL;
                free (tmp->airport.IATA_CODE);
                free (tmp);
            }
        }
    }
}

int is_empty_airlines (Table_hash_airlines thal)
{
    for (int i = 0; i < AIRLINE_SIZE_TABLE; i++)
    {
        if (thal.tab_airlines[i] != NULL)
        {
            return 1;
        }
    }

    return 0;
}

int is_empty_airports (Table_hash_airports thap)
{
    for (int i = 0; i < AIRPORT_SIZE_TABLE; i++)
    {
        if (thap.tab_airports[i] != NULL)
        {
            return 1;
        }
    }

    return 0;
}
