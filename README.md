# Tutorat de Programmation Avancée (SE3 - 2020/2021)

Ce dépôt `GIT` contient l'ensemble du programme codé en C concernant le projet de Programmation Avancée permettant de récupérer des données provenant d'un fichier .csv, de les stocker et d'effectuer des requêtes dessus. Ce programme est utilisé sur les 58592 vols aux États Unis en 2014.

## Contenu

- `Readme.md` : Le fichier que vous êtes en train de lire.
- `makefile` : Fichier permettant la compilation de tous les fichier.
- `.clang-format` : Contient les règles de formatage du code (fichier de M. Dequidt).
- `.gitignore` : Permet d'éviter l'ajout des fichiers temporaires sur le dépôt.

- `initializeData.h` : Définitions des structures et Prototypes des fonctions de récupération des données et de stockage dans les structures.
- `requetes.h` : Prototypes des fonctions des requêtes.

- `initializeData.c` : Fonctions de lectures des fichiers et stockage dans les structures.
- `requetes.c` : Fonctions permettant de répondre aux requêtes de l'utilisateur.
- `main.c` : Programme principal permettant de tout initialiser et d'interagir avec l'utisateur.
- `requetes.txt` : Fichier contenant des exemples de requêtes utilisable avec le programme.

Dans le dossier data :
- `airline.csv` : Fichier de données des compagnies aériennes.
- `airports.csv` : Fichier de données des aéroports.
- `airline.csv` : Fichier de données des vols.



## Compilation

Notre makefile permet la compilation de tous les fichier avec la commande `make` et de créer un executable qui se nommera `main`.

Le compilateur utilisé est clang avec les options -Wall -Wextra et -g pour valgrind.

## Execution et utilisation du programme

Notre programme permet à l'utilisateur de rentrer des requêtes manuellement ou à l'aide de la redirection en entrée.

Une fois la commande `make` exécuté, un fichier main apparaîtra.

Pour exécuter manuellement, il suffit d'exécuter la commande `./main`, d'attendre le chargement des données et de taper les requêtes. Si vous voulez quitter le programme il suffit de rentrer `quit`.

Pour exécuter automatiquement, il faut faire une redirection. Pour cela taper la commande
`./main < monfichier.txt`.

Un fichier `requête.txt` est à votre disposition pour tester le programme.

Les requêtes implémentées sont :

- `show-airports <airline_id>` : affiche tous les aéroports depuis lesquels la compagnie aérienne `<airline_id>` opère des vols
- `show-airlines <port_id>`: affiche les compagnies aériens qui ont des vols qui partent de l'aéroport `<port_id>`
- `show-flights <port_id> <date> [<time>] [limit=<xx>]` : affiche les vols qui partent de l'aéroport à la date, avec optionnellement une heure de début, et limité à xx vols
- `most-delayed-flights` : donne les 5 vols qui ont subis les plus longs retards à l'arrivée
- `most-delayed-airlines` : donne les 5 compagnies aériennes qui ont, en moyenne, le plus de retards
- `delayed-airline <airline_id>` : donne le retard moyen de la compagnie aérienne passée en paramètre
- `most-delayed-airlines-at-airport <airport_id>` : donne les 3 compagnies aériennes avec le plus de retard d'arrivé à l'aéroport passée en paramètre
- `changed-flights <date>` : les vols annulés ou déviés à la date <date> (format M-D)
- `avg-flight-duration <port_id> <port_id>`: calcule le temps de vol moyen entre deux aéroports
- `quit`       : quit
